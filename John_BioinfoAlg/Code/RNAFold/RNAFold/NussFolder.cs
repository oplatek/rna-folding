﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.IO;
using Bio;
namespace RNAFold
{
    class NussFolder : IFoldible
    {
        private ISequence sequence; // The RNA sequence
        private int[,] s;
        int[] viennaD;
        public NussFolder(ISequence Sequence)
        {
            // Initialize      
            sequence = Sequence;
            s = new int[sequence.Count, sequence.Count];
            viennaD = new int[sequence.Count];
        }

        private void buildDP() 
        { 
            // s is full of zeros
            for (int n = 1; n < sequence.Count; n++) 
            {
                for (int j = n; n < sequence.Count; j++)
                {
                    int i = j - n;
                    // cases a, b, c, d
                    int a = s[i + 1, j - 1] + delta(sequence[i], sequence[j]);
                    int b = s[i + 1, j];
                    int c = s[i, j - 1];
                    List<int> options= new List<int>() {a,b,c};
                    if (i + 3 <= j) {
                        for (int k = i + 1; k < j; k++)
                            options.Add(s[i, k] + s[k + 1, j]);
                    }
                    s[i,j] = options.Max();
                }
            }
        }

        private void traceback(int i, int j) 
        {
            if (i >= j) return;
            // now i<j
            if (s[i, j] == s[i + 1, j])
            {
                traceback(i + 1, j);
            }
            else if (s[i, j] == s[i, j - 1])
            {
                traceback(i, j - 1);
            }
            else if (s[i, j] == s[i + 1, j - 1] + delta(sequence[i], sequence[j]))
            {
                traceback(i + 1, j - 1);
                viennaD[i + 1] = 1;
                viennaD[j - 1] = 2;
            }
            else
            {
                for (int k = i + 1; k < j; k++) {
                    if (s[i, j] == s[i, k] + s[k + 1, j]) {
                        traceback(i, k);
                        traceback(k + 1, j);
                    }
                }
            }
            

            

           
        }

        private int delta(byte a, byte b)
        {
          if (a == Bio.Alphabets.RNA.A && b == Bio.Alphabets.RNA.U)
            return 1;
          if (a == Bio.Alphabets.RNA.U && b == Bio.Alphabets.RNA.A)
            return 1;
          if (a == Bio.Alphabets.RNA.G && b == Bio.Alphabets.RNA.C)
            return 1;
          if (a == Bio.Alphabets.RNA.C && b == Bio.Alphabets.RNA.G)
            return 1;
          // else
          return 0;
        }

        public string Fold()
        {

            // Get the result in the Vienna Dot Bracket format
            buildDP();
            this.traceback(0, (int)sequence.Count - 1);

            return viennaDotBracket();
        }

        private string viennaDotBracket()
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < sequence.Count; i++) {
                char c;
                if (viennaD[i] == 0)
                    c = '.';
                else if (viennaD[i] == 1)
                    c = '(';
                else
                    c = ')';                    
                s.Append(c);
            }
            return s.ToString();
        }


        /// <summary>
        /// Comparer for the ISequnces (lexicographic)s.
        /// </summary>
        private class SeqComparer : IComparer<ISequence>
        {
            public int Compare(ISequence s1, ISequence s2)
            {
                int i = 0;
                while (s1.Count > i && s2.Count > i && s1[i] == s2[i])
                    ++i;

                if (i == s2.Count && i == s1.Count)
                    return 0;
                if (i == s1.Count && i < s2.Count)
                    return -1;
                if (i == s2.Count && i < s1.Count)
                    return 1;
                if (s1[i] < s2[i])
                    return -1;
                if (s1[i] > s2[i])
                    return 1;

                return 0;
            }
        }
    }
}
