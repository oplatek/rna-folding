﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bio.IO;
using Bio;
namespace RNAFold
{
  class Folder : IFoldible
  {
    /// <summary>
    /// Working values.
    /// </summary>
    struct WV
    {
      /// <summary>
      /// Energy.
      /// </summary>
      public double e;

      /// <summary>
      /// Origin.
      /// V:
      /// 1 .. hairpin
      /// 2 .. stacked pair
      /// 3 .. bulge
      /// 4 .. inner loop
      /// 5 .. multiloop 
      /// W:
      /// 1 .. W(i+1, j)
      /// 2 .. W(j-1, i)
      /// 3 .. V(j,i)
      /// 4 .. W(i, k) + W(k+1, j)
      /// </summary>
      public byte o;

      /// <summary>
      /// Parameter 1.
      /// </summary>
      public long p1;

      /// <summary>
      /// Parameter 2.
      /// </summary>
      public long p2;

      /// <summary>
      /// Parameter 3. 
      /// </summary>
      public long p3;
    }

    #region tables
    private static double INF = double.PositiveInfinity;

    // Internal loop free energy table
    private static double[] Internal = new double[]{INF, INF, INF, INF, 1.70, 1.80, 2.00, 2.20, 2.30, 2.40,
        2.50, 2.60, 2.70, 2.80, 2.90, 3.00, 3.00, 3.10, 3.10, 3.20, 3.30, 3.30,
        3.40, 3.40, 3.40, 3.50, 3.50, 3.60, 3.60, 3.60, 3.70};
    // Bulge free energy table
    private static double[] Bulge = new double[]{INF, 3.80, 2.80, 3.20, 3.60, 4.00, 4.40, 4.60, 4.70,
        4.80, 4.90, 5.00, 5.10, 5.20, 5.30, 5.40, 5.40, 5.50, 5.50, 5.60, 5.70,
        5.70, 5.80, 5.80, 5.80, 5.90, 5.90, 6.00, 6.00, 6.00, 6.10};
    // Hairpin free energy table
    private static double[] HP = new double[]{INF, INF, INF, 5.70, 5.60, 5.60, 5.40, 5.90, 5.60, 6.40, 6.50, 6.60,
      6.70, 6.80, 6.90, 6.90, 7.00, 7.10, 7.10, 7.20, 7.20, 7.30, 7.30, 7.40,
      7.40, 7.50, 7.50, 7.50, 7.60, 7.60, 7.70};
    // Multiloop energy constants
    private static double a = 10.10, b = -0.30, c = -0.30;

    // Terminal mismatch stacking energies at 37 °C (hairpin loops)
    //
    // 5' --> 3'
    //    WX
    //    ZY
    // 3'<-- 5'
    //
    // Indexed as [W,Z,X,Y]
    double[, , ,] hairpinClosing = new double[,,,]{
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {-0.30, -0.50, -0.30, -0.30,}, {-0.10, -0.20, -1.50, -0.20}, {-1.10, -1.20, -0.20,  0.20}, {-0.30, -0.30, -0.60, -1.10}}
      },
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {-1.50, -1.50, -1.40, -1.80}, {-1.00, -0.90, -2.90, -0.80}, {-2.20, -2.00, -1.60, -1.10}, {-1.70, -1.40, -1.80, -2.00}},
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }      
      },
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {-1.10, -1.50, -1.30, -2.10}, {-1.10, -0.70, -2.40, -0.50}, {-2.40, -2.90, -1.40, -1.20}, {-1.90, -1.00, -2.20, -1.50} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {0.20, -0.50, -0.30, -0.30}, {-0.10, -0.20, -1.50, -0.20}, {-0.90, -1.10, -0.30,  0.00}, {-0.30, -0.30, -0.40, -1.10} },       
      },
      { { {-0.50, -0.30, -0.60, -0.50}, {-0.20, -0.10, -1.20, -0.00}, {-1.40, -1.20, -0.70, -0.20}, {-0.30, -0.10, -0.50, -0.80} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {-0.50, -0.30, -0.60, -0.50}, {-0.20, -0.10, -1.70,  0.00}, {-0.80, -1.20, -0.30, -0.70}, {-0.60, -0.10, -0.60, -0.80} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }
      }};

    // Terminal mismatch stacking energies at 37 °C (internal loops)
    //
    // 5' --> 3'
    //    WX
    //    ZY
    // 3'<-- 5'
    //
    // Indexed as [W,Z,X,Y]
    double[, , ,] internalClosing = new double[,,,]{
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {0.70, 0.70, -0.40, 0.70}, {0.70, 0.70, 0.70, 0.70}, {-0.40, 0.70, 0.70, 0.70}, {0.70, 0.70, 0.70, 0.00}}
      },
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {-0.00, -0.00, -1.10, -0.00}, {-0.00, -0.00, -0.00, -0.00}, {-1.10, -0.00, -0.00, -0.00}, {-0.00, -0.00, -0.00, -0.70} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }
      },
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {-0.00, -0.00, -1.10, -0.00}, {-0.00, -0.00, -0.00, -0.00}, {-1.10, -0.00, -0.00, -0.00}, {-0.00, -0.00, -0.00, -0.70} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {0.70, 0.70, -0.40, 0.70}, {0.70, 0.70, 0.70, 0.70}, {-0.40, 0.70, 0.70, 0.70}, {0.70, 0.70, 0.70, 0.00} }
      },
      { { {0.70, 0.70, -0.40, 0.70}, {0.70, 0.70, 0.70, 0.70}, {-0.40, 0.70, 0.70, 0.70}, {0.70, 0.70, 0.70, 0.00} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {0.70, 0.70, -0.40, 0.70}, {0.70, 0.70, 0.70, 0.70}, {-0.40, 0.70, 0.70, 0.70}, {0.70, 0.70, 0.70, 0.00} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }
      }};

    // Stacking energies at 37 °C
    //
    // 5' --> 3'
    //    WX
    //    ZY
    // 3'<-- 5'
    //
    // Indexed as [W,Z,X,Y]
    double[, , ,] stacking = new double[,,,]{
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {-0.90, 0, 0, 0}, {0, -2.20, 0, 0}, {0, -2.10, 0, -0.60}, {-1.10, 0, -1.40, 0} }
      },
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {0, 0, 0, -2.10}, {0, 0, -3.30, 0}, {0, -2.40, 0, -1.40}, {-2.10, 0, -2.10, 0} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }
      },
      { { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }, 
        { {0, 0, 0, -2.40}, {0, 0, -3.40, 0}, {0, -3.30, 0, -1.50}, {-2.20, 0, -2.50, 0} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {0, 0, 0, -1.30}, {0, 0, -2.50, 0}, {0, -2.10, 0, -0.50}, {-1.40, 0, 1.30, 0} }
      },
      { { {0, 0, 0, -1.30}, {0, 0, -2.40, 0}, {0, -2.10, 0, -1.00}, {-0.90, 0, -1.30, 0} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {0, 0, 0, -1.00}, {0, 0, -1.50, 0}, {0, -1.40, 0, 0.30}, {-0.60, 0, -0.50, 0} },
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF} }
      }
      };

    //  5' --> 3'
    //     YX    
    //     Z     
    //  3' <-- 5'
    //
    // Indexed as [Y,Z,X]
    double[, ,] singleStack21 = new double[,,]{ 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {-0.80, -0.50, -0.80, -0.60} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {-1.70, -0.80, -1.70, -1.20}, {INF, INF, INF, INF} },
        { {INF, INF, INF, INF}, {-1.10, -0.40, -1.30, -0.60}, {INF, INF, INF, INF}, {INF, INF, INF, INF} },
        { {-0.70, -0.10, -0.70, -0.10}, {INF, INF, INF, INF}, {-0.70, -0.10, -0.70, -0.10}, {INF, INF, INF, INF} }
      };

    //  5' --> 3'
    //     Y    
    //     ZX     
    //  3' <-- 5'
    //
    // Indexed as [Y,Z,X]
    double[, ,] singleStack12 = new double[,,]{ 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {INF, INF, INF, INF}, {-0.30, -0.10, -0.20, -0.20} }, 
        { {INF, INF, INF, INF}, {INF, INF, INF, INF}, {-0.20, -0.30, -0.00, -0.00}, {INF, INF, INF, INF} },
        { {INF, INF, INF, INF}, {-0.50, -0.30, -0.20, -0.10}, {INF, INF, INF, INF}, {-0.30, -0.10, -0.20, -0.20} },
        { {-0.30, -0.30, -0.40, -0.20}, {INF, INF, INF, INF}, {-0.30, -0.30, -0.40, -0.20}, {INF, INF, INF, INF} }
      };
    #endregion

    private SortedList<ISequence, double> TLB; // Tetraloop Bonuses table
    private WV[,] wv; // Working values
    private ISequence sequence; // The RNA sequence

    public Folder(ISequence Sequence)
    {
      // Initialize      
      sequence = Sequence;
      wv = new WV[sequence.Count, sequence.Count];

      // TetraLoop Bonuses table
      TLB = new SortedList<ISequence, double>(new SeqComparer());
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGGGAC"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGUGAC"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGAAAG"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGAGAC"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGCAAG"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGAAAC"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGGAAG"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CUUCGG"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGUGAG"), -3.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGAAGG"), -2.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CUACGG"), -2.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGCAAC"), -2.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGCGAG"), -2.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "UGAGAG"), -2.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGAGAG"), -2.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "AGAAAU"), -2.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGUAAG"), -2.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CUAACG"), -2.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "UGAAAG"), -2.00);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGAAGC"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGGAAC"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "UGAAAA"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "AGCAAU"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "AGUAAU"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "CGGGAG"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "AGUGAU"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGCGAC"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GGGAGC"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "GUGAAC"), -1.50);
      TLB.Add(new Sequence(RnaAlphabet.Instance, "UGGAAA"), -1.50);
    }

    public string Fold()
    {
      int i, j, k;
      double e;
      // initialize
      for (i = 0; i < sequence.Count; ++i)
        for (j = i; (j < i + 4 && j < sequence.Count); ++j)
        {
          wv[j, i].e = double.PositiveInfinity;
          wv[i, j].e = double.PositiveInfinity;
        }
            
      // fill the dynamic programming table (compute)
      for (k = 4; k < sequence.Count; ++k)
      {
        // Log progress
        Console.WriteLine("Progress: " + k.ToString() + " / " + sequence.Count.ToString());

        j = k;
        i = 0;
        while (j < sequence.Count)
        {
          // compute V
          wv[j, i].e = double.PositiveInfinity;

          // -> hairpin
          e = hairpinEnergy(sequence.GetSubSequence(i, j - i + 1));
          setVLE(i, j, e, 1);

          int m, n;
          for (m = i + 1; m < j - 4; ++m)
            for (n = m + 4; n < j; ++n)
            {
              // -> stacked pair
              if (m == i + 1 && n == j - 1)
              {
                e = stackedEnergy(sequence[i], sequence[j], sequence[i + 1], sequence[j - 1]);
                e += wv[n, m].e;
                setVLE(i, j, e, 2);
              }
              // -> bulge 
              else if (m == i + 1 || n == j - 1)
              {
                e = bulgeEnergy((m - i - 1) + (j - n - 1), sequence[i], sequence[j], sequence[m], sequence[n]);
                e += wv[n, m].e;
                setVLE(i, j, e, 3, m, n);
              }
              // -> internal loop
              else
              {
                e = internalEnergy((m - i - 1) + (j - n - 1), sequence[i], sequence[j], sequence[i + 1], sequence[j - 1],
                  sequence[n], sequence[m], sequence[n+1], sequence[m-1]);
                e += wv[n, m].e;
                setVLE(i, j, e, 4, m, n);
              }
            }
          // -> multiloop
          for (int q = i + 4; q < j - 3; ++q)
          {
            // (energy of the 1st part) + (energy of the 2nd part) +
            // (offset constant) + (free base penalty) * (# of free bases in both parts) +
            // (helix penalty) * (# of pairs in both parts)
            e = wv[i + 1, q].e + wv[q + 1, j - 1].e + a + b * (wv[i + 1, q].p2 + wv[q + 1, j - 1].p2) +
               c * (wv[i + 1, q].p3 + wv[q + 1, j - 1].p3);
            setVLE(i, j, e, 5, q);
          }

          // compute W
          wv[i, j].e = double.PositiveInfinity;
          // -> W(i+1, j)
          setWLE(i, j, wv[i + 1, j].e, 1, 0, wv[i + 1, j].p2 + 1, wv[i + 1, j].p3);
          // -> W(i, j-1)
          setWLE(i, j, wv[i, j - 1].e, 2, 0, wv[i + 1, j].p2 + 1, wv[i + 1, j].p3);
          // -> V(i, j)
          setWLE(i, j, wv[j, i].e, 3, 0, 0, 1);
          // -> min_k { W(i, q) + W(q+1, j) }
          for (int q = i + 4; q < j - 3; ++q)
            setWLE(i, j, wv[i, q].e + wv[q + 1, j].e, 4, q, wv[i, q].p2 + wv[q + 1, j].p2,
               wv[i, q].p3 + wv[q + 1, j].p3);

          ++i;
          ++j;
        }
      }
      
      // Get the result in the Vienna Dot Bracket format
      StringBuilder str = new StringBuilder();
      viennaDotBracket(0, sequence.Count - 1, str);
      str.AppendLine();
      str.AppendLine("Enargy: " + wv[0, sequence.Count-1].e.ToString());

      return str.ToString();
    }

    private StringBuilder viennaDotBracket(long i, long j, StringBuilder str)
    {
      // infix

      // W
      if (i < j)
      {
        switch (wv[i, j].o)
        {
          // -> W(i+1, j) 
          case 1:
            str.Append('.');
            viennaDotBracket(i + 1, j, str);
            break;
          // -> W(i, j-1)
          case 2:
            viennaDotBracket(i, j-1, str);
            str.Append('.');
            break;
          // -> V(i, j)
          case 3:
            viennaDotBracket(j, i, str);
            break;
          // -> min_k { W(i, q) + W(q+1, j) }
          case 4:
            StringBuilder sb = new StringBuilder();
            viennaDotBracket(i, wv[i, j].p1, str);
            viennaDotBracket(wv[i, j].p1 + 1, j, sb);
            str.Append(sb);
            break;
        }
      }
      // V
      else
      {
        switch (wv[i, j].o)
        {
          // -> hairpin
          case 1:
            str.Append('(');
            for (long d = j + 1; d < i; ++d)
              str.Append('.');
            str.Append(')');
            break;
          // -> stacked pair
          case 2:
            str.Append('(');
            viennaDotBracket(i - 1, j + 1, str);
            str.Append(')');
            break;
          // -> bulge, internal loop
          case 3:
          case 4:
            // (
            str.Append('(');
            //  left ...
            for (long d = j + 1; d < wv[i, j].p1; ++d)
              str.Append('.');
            // inner ()
            viennaDotBracket(wv[i, j].p2, wv[i, j].p1, str);
            // right ...
            for (long d = wv[i, j].p2 + 1; d < i; ++d)
              str.Append('.');
            // )
            str.Append(')');
            break;
          // -> multiloop
          case 5:
            // (
            str.Append('(');
            // the first part
            viennaDotBracket(j + 1, wv[i, j].p1, str);
            // the second part
            StringBuilder sb = new StringBuilder();
            viennaDotBracket(wv[i, j].p1 + 1, i - 1, sb);
            str.Append(sb);
            // )
            str.Append(')');
            break;
        }
      }
      return str;
    }

    /// <summary>
    /// Set the values to V if the energy is lower.
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="e"></param>
    /// <param name="o"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    private void setVLE(long i, long j, double e, byte o, long p1 = 0, long p2 = 0, long p3 = 0)
    {
      if (e >= wv[j, i].e)
        return;
      wv[j, i].e = e;
      wv[j, i].o = o;
      wv[j, i].p1 = p1;
      wv[j, i].p2 = p2;
      wv[j, i].p3 = p3;
    }

    /// <summary>
    /// Set the values to W if the energy is lower.
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="e"></param>
    /// <param name="o"></param>
    /// <param name="p1"></param>
    /// <param name="p2"></param>
    private void setWLE(long i, long j, double e, byte o, long p1 = 0, long p2 = 0, long p3 = 0)
    {
      if (e > wv[i, j].e)
        return;
      wv[i, j].e = e;
      wv[i, j].o = o;
      wv[i, j].p1 = p1;
      wv[i, j].p2 = p2;
      wv[i, j].p3 = p3;
    }

    /// <summary>
    /// Comparer for the ISequnces (lexicographic)s.
    /// </summary>
    private class SeqComparer : IComparer<ISequence>
    {
      public int Compare(ISequence s1, ISequence s2)
      {
        int i = 0;
        while (s1.Count > i && s2.Count > i && s1[i] == s2[i])
          ++i;

        if (i == s2.Count && i == s1.Count)
          return 0;
        if (i == s1.Count && i < s2.Count)
          return -1;
        if (i == s2.Count && i < s1.Count)
          return 1;
        if (s1[i] < s2[i])
          return -1;
        if (s1[i] > s2[i])
          return 1;

        return 0;
      }
    }

    public static byte NucleotideToZeroBasedByte(byte b)
    {
      if (b == Bio.Alphabets.RNA.A)
        return 0;
      if (b == Bio.Alphabets.RNA.C)
        return 1;
      if (b == Bio.Alphabets.RNA.G)
        return 2;
      if (b == Bio.Alphabets.RNA.U)
        return 3;
      throw new Exception();
    }

    public static byte ZeroBasedByteToNucleotide(byte b)
    {
      if (b == 0)
        return Bio.Alphabets.RNA.A;
      if (b == 1)
        return Bio.Alphabets.RNA.C;
      if (b == 2)
        return Bio.Alphabets.RNA.G;
      if (b == 3)
        return Bio.Alphabets.RNA.U;
      throw new Exception();
    }

    /// <summary>
    /// Get the free energy of a hairpin.
    /// </summary>
    /// <param name="seq">Closing pair including.</param>
    /// <returns></returns>
    private double hairpinEnergy(ISequence seq)
    {
      long len = seq.Count - 2;
      double energy = 0;

      // Shorter than 3 doesn't occure
      if (len < 3)
        return double.PositiveInfinity;

      // 3-loop hairpin doesn't have closing pair bonus
      if (len == 3)
      {
        energy += HP[len];
        // triloop special bonus
        if (seq.GetSubSequence(1, 3).Equals(new Sequence(RnaAlphabet.Instance, "GGG")))
          energy += -2.20;
        return energy;
      }

      // 4-loop hairpin
      if (len == 4)
      {
        energy += HP[len];
        // tetraloop special bonuses
        if (TLB.ContainsKey(seq))
          energy += TLB[seq];
      }
      else if (len <= 30)
      {
        energy += HP[len];
      }
      else
      {
        energy += HP[30];
        // 1.75 * R * T * ln(size/30)
        energy += 1.75 * 8.3144621 * 310.15 * Math.Log(len / 30.0);
      }

      // add closing pair bonus
      energy += hairpinClosing[
        NucleotideToZeroBasedByte(seq[0]),
        NucleotideToZeroBasedByte(seq[seq.Count - 1]),
        NucleotideToZeroBasedByte(seq[1]),
        NucleotideToZeroBasedByte(seq[seq.Count - 2])];

      return energy;
    }

    /// <summary>
    /// Indexed as [W,Z,X,Y]
    /// </summary>
    /// <returns></returns>
    private double stackedEnergy(byte W, byte Z, byte X, byte Y)
    {
      return stacking[
        NucleotideToZeroBasedByte(W),
        NucleotideToZeroBasedByte(Z),
        NucleotideToZeroBasedByte(X),
        NucleotideToZeroBasedByte(Y)];
    }


    /// <summary>
    /// o - the outer closing pair, i - inner closing pair. Both indexed as [W,Z,X,Y].
    /// </summary>
    /// <param name="len"></param>
    /// <param name="oW"></param>
    /// <param name="bZ"></param>
    /// <param name="bX"></param>
    /// <param name="bY"></param>
    /// <param name="eW"></param>
    /// <param name="eZ"></param>
    /// <param name="eX"></param>
    /// <param name="eY"></param>
    /// <returns></returns>
    private double internalEnergy(long len, byte oW, byte oZ, byte oX, byte oY, byte iW, byte iZ, byte iX, byte iY)
    {
      double energy = 0;

      // small internal loops
      if (len == 2)
        return 0.4;
      // TODO: add tables for 1x2, 2x2, and 2x3 loops

      if (len <= 30)
      {
        energy += Internal[len];
      }
      else
      {
        energy += Internal[30];
        // 1.75 * R * T * ln(size/30)
        energy += 1.75 * 8.3144621 * 310.15 * Math.Log(len / 30.0);
      }

      // the first closing pair
      energy += internalClosing[
        NucleotideToZeroBasedByte(oW),
        NucleotideToZeroBasedByte(oZ),
        NucleotideToZeroBasedByte(oX),
        NucleotideToZeroBasedByte(oY)];
      // the second closing pair
      energy += internalClosing[
        NucleotideToZeroBasedByte(iW),
        NucleotideToZeroBasedByte(iZ),
        NucleotideToZeroBasedByte(iX),
        NucleotideToZeroBasedByte(iY)];

      return energy;
    }

    /// <summary>
    /// Indexed as [W,Z,X,Y].
    /// </summary>
    /// <param name="len"></param>
    /// <param name="W"></param>
    /// <param name="Z"></param>
    /// <param name="X"></param>
    /// <param name="Y"></param>
    /// <returns></returns>
    private double bulgeEnergy(long len, byte W, byte Z, byte X, byte Y)
    {
      double energy = 0;

      if (len == 1)
        energy += stacking[
        NucleotideToZeroBasedByte(W),
        NucleotideToZeroBasedByte(Z),
        NucleotideToZeroBasedByte(X),
        NucleotideToZeroBasedByte(Y)];

      if (len <= 30)
        energy += Bulge[len];
      else
      {
        energy += Bulge[30];
        energy += 1.75 * 8.3144621 * 310.15 * Math.Log(len / 30.0);
      }

      return energy;
    }
  }
}
