﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Bio.IO;
using Bio;

namespace RNAFold
{
    interface IFoldible {
        string Fold();
    }
  class Program
  {

    /// <summary>
    /// The sequence source file.
    /// </summary>
    //public static string SourcePath = @"D:\School\MFF\Mgr\Bioinformatika\Zapoctak\EperimentalData\Exp1.fasta";
    
    static void Main(string[] args)
    {
      if (args.Length < 1)
      {
        ShowErr("Eroor: No file path argument found.");
        return;
      }        
            
      string SourcePath = args[0];

      if(!System.IO.File.Exists(SourcePath))
      {
        ShowErr("Eroor: The specified file was not found.");
        return;
      }

      // Load a sequence from a file
      IEnumerable<ISequence> Sequences;
      if (!LoadSeqs(SourcePath, out Sequences))
        return;

      ISequence seq = Sequences.First();

      if (seq.Alphabet.GetType() != typeof(RnaAlphabet))
      {
        ShowErr("Eroor: Not an RNA sequence.");
        return;
      }

      if (seq == null)
      {
        ShowErr("Error: Can't find any sequence in the file.");
        return;
      }

      // Computation
      System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
      //IFoldible folder = new Folder(seq);
      IFoldible folder = new NussFolder(seq);
      stopwatch.Start();
      string str = folder.Fold();
      stopwatch.Stop();
      // Output: vienna dot bracket notation (http://rna.tbi.univie.ac.at/help.html#A6)
           
      string resPath = System.IO.Path.GetDirectoryName(SourcePath) + "\\" +
        System.IO.Path.GetFileNameWithoutExtension(SourcePath) + ".txt";
      System.IO.StreamWriter sw = new StreamWriter(resPath);

      for (int i = 0; i < seq.Count; ++i)
      {
        char c = ByteToBaseChar(seq[i]);
        sw.Write(c);
        Console.Write(c);
      }
      sw.WriteLine();
      Console.WriteLine();

      Console.WriteLine(str);
      sw.WriteLine(str);
      sw.Close();

      Console.WriteLine("Elapsed time: " + stopwatch.ElapsedMilliseconds + "ms");
      Console.WriteLine("Press any key to quit...");
      Console.ReadKey();   
    }

    public static char ByteToBaseChar(byte b)
    {
      if (b == Bio.Alphabets.RNA.A)
        return 'A';
      if (b == Bio.Alphabets.RNA.C)
        return 'C';
      if (b == Bio.Alphabets.RNA.G)
        return 'G';
      if (b == Bio.Alphabets.RNA.U)
        return 'U';
      return '-';
    }

    /// <summary>
    /// Load the sequences.
    /// </summary>
    /// <param name="SourcePath">The path to a source file.</param>
    /// <param name="Sequences">An output variable for an enumerable of sequences.</param>
    /// <returns>Whether successful.</returns>
    private static bool LoadSeqs(string SourcePath, out IEnumerable<ISequence> Sequences)
    {
      Sequences = null;

      // Get the file name extension
      string fileExtension = Path.GetExtension(SourcePath);
      if (string.IsNullOrEmpty(fileExtension))
      {
        ShowErr("Can't read the source file.");
        return false;
      }
      // Select the right parser based on the file path
      ISequenceParser parser = SequenceParsers.FindParserByFileName(SourcePath) ??
                                 SequenceParsers.All.FirstOrDefault(sp => sp.SupportedFileTypes.Contains(fileExtension));
      if (parser == null)
      {
        ShowErr("Can't find an appropriate parser.");
        return false;
      }
      
      // Parse the sequences in the file
      try { Sequences = parser.Parse(); }
      catch
      {
        ShowErr("Error when parsing the sequences.");
        return false;
      }

      return true;
    }

    /// <summary>
    /// Show the error message and wait for a keypress.
    /// </summary>
    /// <param name="msg"></param>
    private static void ShowErr(string msg)
    {
      Console.WriteLine();
      Console.WriteLine(msg + " Press any key to continue...");
      Console.ReadKey();
    }

  }
}
