using System;
using System.Text;
using System.IO;
using System.Diagnostics; // Debug

namespace rnanussen
{
	class MainClass
	{
		public static string readFirstFasta (TextReader r)
		{
			string line = r.ReadLine ();
			Debug.Assert (line [0] != '>', "First line of Fasta format should contain description");
			StringBuilder sr = new StringBuilder();
			while ((line = r.ReadLine()) != null) {
				sr.Append(line);
			}
			return sr.ToString();
		}

		public static void Main (string[] args)
		{
            Console.WriteLine("Input: RNA ve formatu FASTA \n" +
            	"Output: Vienna Dot Bracket ( http://rna.tbi.univie.ac.at/help.html#A6 )");
			Console.WriteLine (args[0]);
			using (TextReader r = new StreamReader(args[0])) {
				string fasta = readFirstFasta(r);
				Console.WriteLine (fasta);
			}
			Console.WriteLine ("TODO read the paper and code it");
		}
	}
}
